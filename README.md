# Minecraft EDU Admin
A web based admin interface to the Jersey STEM minecraft Edu server. 

## Web Structure
 - Index - Overview of options
 - Profile - Edit my own user account
 - User Admin (create/delete admin users)
 - Player Management (may or may not do this, to replace existing web form)
 - Server (instance) Status
   - Edit Server
     - 'Lock/Unlock' instance (enable/disable student access)
     - Link world
     - Server (instance) Status 
       - online/offline, port, current world, # active players
       - student lock/open
       - Login whitelist (readonly)
         - Force update/download
       - Stop/Start/Restart instance
  - Mod Management
    - List current Mods
    - Enable/Disable mod
    - Install new mod
    - Download all active mods
  - World Management
     - Install new world
     - Restore World
     - Clone World
  - Online Help (contextual and throughout)


